import { StatusInterface } from "./StatusInterface";
import { UserInterface } from "./UserInterface";

export interface TimesheetInterface {
  id: number,
  date_from?: Date,
  date_to?: Date,
  status: StatusInterface,
  user: UserInterface,
  projectName: string,
  task: string
}
export interface StatusInterface {
  id: number,
  description: string,
  status: string
}
import { TimesheetInterface } from "./TimesheetInterface";

export interface DialogDataInterface {
  mode: 'create' | 'update',
  form: TimesheetInterface
}
import { Component, OnInit } from '@angular/core';
import { TaskSearchComponent } from './components/task-search/task-search.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { CommonModule } from '@angular/common';
import { Observable, ReplaySubject, filter } from 'rxjs';
import { TimesheetInterface } from './interfaces/TimesheetInterface';
import { TimesheetService } from './services/timesheet.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    TaskSearchComponent,
    TaskListComponent,
    CommonModule,
    HttpClientModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  providers: [TimesheetService],
})
export class AppComponent implements OnInit {
  title = 'demo-frontend';

  timesheets: TimesheetInterface[] = [];
  subject$: ReplaySubject<TimesheetInterface[]> = new ReplaySubject<
    TimesheetInterface[]
  >(1);
  data$: Observable<TimesheetInterface[]> = this.subject$.asObservable();

  constructor(private timesheetServ: TimesheetService) {}

  ngOnInit(): void {
    this.populateTimesheet();
    this.data$.pipe(filter((data) => !!data)).subscribe((timesheets) => {
      this.timesheets = timesheets;
    });
  }

  populateTimesheet() {
    this.timesheetServ.getTimesheets().subscribe((timesheets) => {
      if (timesheets !== null) {
        this.subject$.next(timesheets);
      }
    });
  }

  updateTimesheet($event: Boolean) {
    // this.timesheets = $event;
    if ($event) {
      this.populateTimesheet();
    }
  }

  searchTask($event: String) {
    this.timesheetServ.getTimesheetsQuery($event).subscribe((timesheets) => {
      if (timesheets !== null) {
        this.subject$.next(timesheets);
      }
    });
  }
}

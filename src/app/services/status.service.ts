import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { StatusInterface } from '../interfaces/StatusInterface';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StatusService {
  constructor(private http: HttpClient) {}

  env = environment.backend;

  getStatus() {
    return this.http
      .get<StatusInterface[]>(this.env + '/api/v1/status', {
        // observe: 'response' as 'response',
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      })
      .pipe(map((res) => res));
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { TimesheetInterface } from '../interfaces/TimesheetInterface';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TimesheetService {
  constructor(private http: HttpClient) {}

  env = environment.backend;

  getTimesheets() {
    return this.http
      .get<TimesheetInterface[]>(this.env + '/api/v1/timesheet', {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      })
      .pipe(map((res) => res));
  }

  getTimesheetsQuery(querySearch: String) {
    return this.http
      .get<TimesheetInterface[]>(
        this.env + `/api/v1/timesheet/findTimesheet?query=${querySearch}`,
        {
          headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        }
      )
      .pipe(map((res) => res));
  }

  async addingTimesheet(
    timesheet: TimesheetInterface,
    userId: number,
    statusId: number
  ) {
    return this.http.post<TimesheetInterface>(
      this.env + '/api/v1/timesheet',
      timesheet,
      {
        params: {
          userId: userId,
          statusId: statusId,
        },
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response' as 'response',
      }
    );
  }

  async updateTimesheet(
    timesheet: TimesheetInterface,
    timesheetId: number,
    userId: number,
    statusId: number
  ) {
    return this.http
      .put<any>(
        this.env + `/api/v1/timesheet/update/${timesheetId}`,
        timesheet,
        {
          params: {
            userId: userId,
            statusId: statusId,
          },
          headers: new HttpHeaders({
            Accept: 'application/json',
          }),
          observe: 'response' as 'response',
        }
      )
      .pipe(map((element) => element.body));
  }

  async removeTimesheet(id: number) {
    return this.http
      .delete(this.env + `/api/v1/timesheet/delete/${id}`, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response' as 'response',
      })
      .pipe(map((element) => element.status));
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { StatusInterface } from '../interfaces/StatusInterface';
import { map } from 'rxjs';
import { UserInterface } from '../interfaces/UserInterface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  env = environment.backend;

  getUsers() {
    return this.http
      .get<UserInterface[]>(this.env + '/api/v1/user', {
        // observe: 'response' as 'response',
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      })
      .pipe(map((res) => res));
  }
}

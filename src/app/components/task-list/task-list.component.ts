import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { TimesheetInterface } from '../../interfaces/TimesheetInterface';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { ListColumn } from '../../interfaces/ListColumn';
import { CommonModule } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { CreateUpdateTaskComponent } from '../create-update-task/create-update-task.component';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarModule,
} from '@angular/material/snack-bar';
import { TimesheetService } from '../../services/timesheet.service';

@Component({
  selector: 'task-list',
  standalone: true,
  imports: [
    MatTableModule,
    CommonModule,
    MatPaginator,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
  ],
  templateUrl: './task-list.component.html',
  styleUrl: './task-list.component.css',
})
export class TaskListComponent implements OnInit, OnChanges, AfterViewInit {
  constructor(
    private matDialog: MatDialog,
    private snackbar: MatSnackBar,
    private timesheetService: TimesheetService
  ) {}

  @Input() timesheets: TimesheetInterface[] = [];
  @Output() updateEvents = new EventEmitter<Boolean>();

  columns: ListColumn[] = [
    { name: 'Project Name', property: 'projectName', visible: true },
    { name: 'Task', property: 'task', visible: true },
    { name: 'Assigned To', property: 'username', visible: true },
    { name: 'From', property: 'date_from', visible: true },
    { name: 'To', property: 'date_to', visible: true },
    { name: 'Status', property: 'status', visible: true },
    { name: 'Actions', property: 'actions', visible: true },
  ] as ListColumn[];

  dataSource: MatTableDataSource<TimesheetInterface> = new MatTableDataSource();

  @ViewChild(MatPaginator, {
    static: true,
  })
  paginator: MatPaginator;
  @ViewChild(MatSort, {
    static: true,
  })
  sort: MatSort;
  pageSize = 10;

  ngOnInit(): void {
    // console.log("Timesheet", this.timesheets)
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      const chng = changes[propName];
      this.dataSource.data = chng.currentValue;
      this.timesheets = chng.currentValue;
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  get visibleColumns() {
    return this.columns
      .filter((column) => column.visible)
      .map((column) => column.property);
  }

  updateDialog(timesheet: TimesheetInterface) {
    const config: MatSnackBarConfig = {
      horizontalPosition: 'left',
      duration: 3000,
    };

    this.matDialog
      .open(CreateUpdateTaskComponent, {
        data: {
          mode: 'update',
          form: timesheet,
        },
      })
      .afterClosed()
      .subscribe((data: Boolean) => {
        if (data !== null) {
          this.updateEvents.emit(data);
        } else {
          this.snackbar.open('Failed to add timesheet.', '', config);
          this.updateEvents.emit(false);
        }
      });
  }

  async deleteTimesheet(row: TimesheetInterface) {
    const config: MatSnackBarConfig = {
      horizontalPosition: 'left',
      duration: 3000,
    };

    await this.timesheetService
      .removeTimesheet(row.id)
      .then((obs) => {
        obs.subscribe(
          (ok) => {
            this.timesheets = this.timesheets.filter(
              (each) => each.id !== row.id
            );
            this.updateEvents.emit(true);
            this.snackbar.open('Successfully deleted timesheet.', '', config);
          },
          (err) => {
            console.log(err);
            this.snackbar.open('Failed to delete. Try again', '', config);
          }
        );
      })
      .catch(() => {
        this.snackbar.open('Failed to delete. Try again', '', config);
      });
  }
}

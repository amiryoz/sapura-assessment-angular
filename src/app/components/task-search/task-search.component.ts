import { TimesheetService } from './../../services/timesheet.service';
import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormField,
  MatLabel,
} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CreateUpdateTaskComponent } from '../create-update-task/create-update-task.component';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarModule,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-task-search',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormField,
    MatLabel,
    MatInputModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatButtonModule,
    MatSnackBarModule,
  ],
  templateUrl: './task-search.component.html',
  styleUrl: './task-search.component.css',
  providers: [
    TimesheetService,
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        subscriptSizing: 'dynamic',
      },
    },
  ],
})
export class TaskSearchComponent implements OnInit {
  @Output() updateEvents = new EventEmitter<Boolean>();
  @Output() queryEvents = new EventEmitter<String>();
  searchForm: FormGroup;

  constructor(
    private snackbar: MatSnackBar,
    private matDialog: MatDialog,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.searchForm = this.fb.group({
      search: [null],
    });
  }
  search() {
    this.queryEvents.emit(this.searchForm.controls['search'].value);
  }

  openForm() {
    const config: MatSnackBarConfig = {
      horizontalPosition: 'left',
      duration: 3000,
    };

    this.matDialog
      .open(CreateUpdateTaskComponent, {
        data: {
          mode: 'create',
          form: null,
        },
      })
      .afterClosed()
      .subscribe((data: Boolean) => {
        if (data !== null) {
          this.updateEvents.emit(data);
        } else {
          this.snackbar.open('Failed to add timesheet.', '', config);
          this.updateEvents.emit(false);
        }
      });
  }
}

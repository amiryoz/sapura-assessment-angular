import { TimesheetService } from './../../services/timesheet.service';
import { CommonModule } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import {
  MatFormField,
  MatFormFieldModule,
  MatLabel,
} from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { TimesheetInterface } from '../../interfaces/TimesheetInterface';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { provideNativeDateAdapter } from '@angular/material/core';
import { StatusInterface } from '../../interfaces/StatusInterface';
import { StatusService } from '../../services/status.service';
import { UserService } from '../../services/user.service';
import { map, Observable, ReplaySubject } from 'rxjs';
import { MatSelect, MatSelectModule } from '@angular/material/select';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { UserInterface } from '../../interfaces/UserInterface';
import { DialogDataInterface } from '../../interfaces/DialogDataInterface';

@Component({
  selector: 'app-create-update-task',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormField,
    MatLabel,
    MatInputModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSelect,
    MatButtonModule,
  ],
  templateUrl: './create-update-task.component.html',
  styleUrl: './create-update-task.component.css',
  providers: [
    provideNativeDateAdapter(),
    StatusService,
    UserService,
    TimesheetService,
  ],
})
export class CreateUpdateTaskComponent implements OnInit {
  form: FormGroup;
  status$: ReplaySubject<StatusInterface[]> = new ReplaySubject<
    StatusInterface[]
  >(1);
  statusData$: Observable<StatusInterface[]> = this.status$.asObservable();

  user$: ReplaySubject<UserInterface[]> = new ReplaySubject<UserInterface[]>(1);
  userData$: Observable<UserInterface[]> = this.user$.asObservable();

  mode: 'create' | 'update' = 'create';
  formData: TimesheetInterface = this.defaults.form;
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: DialogDataInterface,
    private dialogRef: MatDialogRef<CreateUpdateTaskComponent>,

    private snackbar: MatSnackBar,
    private fb: FormBuilder,
    private statusService: StatusService,
    private userService: UserService,
    private timesheetService: TimesheetService
  ) {}

  ngOnInit(): void {
    this.mode = this.defaults.mode;

    this.populateServices();

    this.form = this.fb.group({
      task: [null],
      projectName: [null],
      date_from: [null],
      date_to: [null],
      status: [null],
      user: [null],
    });

    if (this.mode == 'update') {
      let patchValues = {
        task: this.defaults.form.task,
        projectName: this.defaults.form.projectName,
        date_from: this.defaults.form.date_from,
        date_to: this.defaults.form.date_to,
        status: this.defaults.form.status.id,
        user: this.defaults.form.user.id,
      };

      this.form.patchValue(patchValues);
    }
  }

  populateServices() {
    this.statusService.getStatus().subscribe((status) => {
      if (status !== null) {
        this.status$.next(status);
      }
    });

    this.userService.getUsers().subscribe((users) => {
      if (users !== null) {
        this.user$.next(users);
      }
    });
  }

  async updateForm() {
    const timesheetId = this.defaults.form.id;

    const config: MatSnackBarConfig = {
      horizontalPosition: 'left',
      duration: 3000,
    };
    const statusId = this.form.controls['status'].value;
    const userId = this.form.controls['user'].value;

    this.form.removeControl('status');
    this.form.removeControl('user');

    this.timesheetService
      .updateTimesheet(this.form.value, timesheetId, userId, statusId)
      .then((ok) => {
        ok.subscribe(
          (obs) => {
            this.snackbar.open('Successfully updating timesheet.', '', config);
            this.dialogRef.close(true);
          },
          (err) => {
            this.snackbar.open('Failed to update', '', config);
            console.log('ERROR PUT: ', err);
          },
          () => {}
        );
      })
      .catch((err) => {
        this.snackbar.open('Failed to update timesheet.', '', config);
      })
      .finally(() => {});
  }

  close() {
    this.dialogRef.close(false);
  }

  async createForm() {
    const config: MatSnackBarConfig = {
      horizontalPosition: 'left',
      duration: 3000,
    };
    const statusId = this.form.controls['status'].value;
    const userId = this.form.controls['user'].value;

    this.form.removeControl('status');
    this.form.removeControl('user');

    this.timesheetService
      .addingTimesheet(this.form.value, userId, statusId)
      .then((ok) => {
        ok.subscribe((obs) => {
          this.snackbar.open('Successfully adding timesheet.', '', config);
          this.dialogRef.close(true);
        });
      })
      .catch((err) => {
        this.snackbar.open('Failed to add timesheet.', '', config);
      });
  }
}
